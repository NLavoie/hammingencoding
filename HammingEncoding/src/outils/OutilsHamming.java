package outils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@SuppressWarnings("javadoc")
public class OutilsHamming {
	private static BitMatrix encoder = null;
	private static BitMatrix controler = null;
	private static BitMatrix decoder = null;
	private static boolean initialized = false;

	private static void initialize() {
		if (!initialized) {
			initialized = true;
			encoder = new BitMatrix(new boolean[][] { { true, true, true, false, false, false, false },
					{ true, false, false, true, true, false, false }, { false, true, false, true, false, true, false },
					{ true, true, false, true, false, false, true } });
			controler = new BitMatrix(new boolean[][] { { false, false, true }, { false, true, false },
					{ false, true, true }, { true, false, false }, { true, false, true }, { true, true, false },
					{ true, true, true } });
			decoder = new BitMatrix(new boolean[][] { { false, false, false, false }, { false, false, false, false },
					{ true, false, false, false }, { false, false, false, false }, { false, true, false, false },
					{ false, false, true, false }, { false, false, false, true }, });
		}
	}

	public static boolean verifyBit(String input) {
		boolean result = true;
		input = input.replaceAll(" ", "");
		input = input.replaceAll("\n", "");
		int i = 0;
		while (result && i < input.length()) {
			char c = input.charAt(i);
			if (c != '1' && c != '0') {
				result = false;
			}
			i++;
		}
		return result;
	}

	public static String decode(String input) {
		initialize();
		String output = "Wrong character";
		if (verifyBit(input)) {
			output = "Wrong size";
			if (input.length() % 14 == 0 && !input.isEmpty()) {
				output = "";
				BitMatrix prevDecoded = null;
				byte byteArray[] = new byte[input.length() / 14];
				for (int i = 0; i < input.length(); i += 7) {
					BitMatrix encoded = new BitMatrix(1, 7);
					for (int b = 0; b < 7; b++) {
						encoded.getMatrix()[0][b] = input.charAt(i + b) == '1' ? true : false;
					}

					BitMatrix check = encoded.multiply(controler);
					if (!check.isNull()) {
						int errorPos = OutilsVarious.uint3FromBits(check.getMatrix()[0]);
						encoded.getMatrix()[0][errorPos - 1] = !encoded.getMatrix()[0][errorPos - 1];
					}

					BitMatrix decoded = encoded.multiply(decoder);
					if (prevDecoded != null) {
						boolean array[] = new boolean[8];
						for (int b = 0; b < 4; b++) {
							array[4 + b] = decoded.getMatrix()[0][b];
							array[b] = prevDecoded.getMatrix()[0][b];
						}
						byteArray[i / 14] = (byte) OutilsVarious.uint8FromBits(array);
						//output += OutilsVarious.uint8FromBits(array);
						prevDecoded = null;
					} else {
						prevDecoded = decoded;
					}
				}
				StringBuilder result = new StringBuilder();
				for (byte aChar : byteArray) {
					result.append(String.format("%8s", Integer.toBinaryString(aChar& 0xFF)) // char -> int, auto-cast
							.replaceAll(" ", "0") // zero pads
					);
				}
				try {
					output = new String(byteArray, "Windows-1252");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return output;
	}

	public static String encode(String input, int errorChance) {
		initialize();
		String output = "";
		String binary = OutilsVarious.convertStringToBinary(input);
		boolean[] array = new boolean[binary.length()];
		for (int i = 0; i < array.length; i++) {
			array[i] = binary.charAt(i) == '1' ? true : false;
		}
		BitMatrix halfChar = new BitMatrix(1, 4);
		for (int i = 0; i < array.length; i += 4) {
			int errorIndex = (int) (Math.random() * 100.0 < errorChance ? Math.random() * 8 : -1);
			for (int b = 0; b < 4; b++) {
				halfChar.getMatrix()[0][b] = array[i + b];
			}
			BitMatrix encoded = halfChar.multiply(encoder);
			for (int b = 0; b < 7; b++) {
				if (errorIndex == b) {
					output += encoded.getMatrix()[0][b] == true ? "0" : "1";
				} else {
					output += encoded.getMatrix()[0][b] == true ? "1" : "0";

				}
			}
		}
		return output;
	}
}
