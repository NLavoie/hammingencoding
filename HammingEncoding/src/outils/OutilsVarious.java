package outils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("javadoc")
public class OutilsVarious {
	public static int uint3FromBits(boolean bits[]) {
		int res = 0;
		for (int i = 0; i < 3; ++i) {
			if (bits[i]) {
				res |= 1 << (2 - i);
			}
		}
		return res;
	}

	public static byte uint8FromBits(boolean bits[]) {
		byte res = 0;
		for (int i = 0; i < 8; ++i) {
			if (bits[i]) {
				res |= 1 << (7 - i);
			}
		}
		return res;
	}

	public static String ascii_to_utf8(String input) {
		String temp = "";
		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) < 0) {
				char c = (char) (input.charAt(i));
				temp += 0xC0 | (c >> 6);
				temp += 0x80 | (c & 0x3f);
			} else {
				temp += input.charAt(i);
			}
		}
		return temp;
	}

	public static String convertStringToBinary(String input) {
		StringBuilder result = new StringBuilder();
		byte[] utf8;
		try {
			utf8 = input.getBytes("Windows-1252"); // StandardCharsets.UTF_8
			for (byte aChar : utf8) {
				result.append(String.format("%8s", Integer.toBinaryString(aChar & 0xFF)) // char -> int, auto-cast
						.replaceAll(" ", "0") // zero pads
				);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result.toString();
	}

	public static String prettyBinary(String binary, int blockSize, String separator) {

		List<String> result = new ArrayList<>();
		int index = 0;
		while (index < binary.length()) {
			result.add(binary.substring(index, Math.min(index + blockSize, binary.length())));
			index += blockSize;
		}

		return result.stream().collect(Collectors.joining(separator));
	}

	public static void saveToClipboard(String string) {
		StringSelection selection = new StringSelection(string);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
	}

	public static String pasteFromClipboard() {
		String output = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				output = (String) contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return output;
	}
}
