package outils;

@SuppressWarnings("javadoc")
public class BitMatrix {
	private boolean[][] matrix = null;

	public BitMatrix(int nbLigne, int nbColonne) {
		matrix = new boolean[nbLigne][nbColonne];
	}

	public BitMatrix(boolean[][] matrix) {
		this.matrix = matrix;
	}

	public boolean isNull() {
		boolean result = true;
		if (matrix != null) {
			for (int l = 0; l < matrix.length; l++) {
				for (int c = 0; c < matrix[l].length; c++) {
					if (matrix[l][c] == true) {
						result = false;
					}
				}
			}
		}
		return result;
	}

	public BitMatrix multiply(BitMatrix second) {
		BitMatrix result = null;
		if (this.matrix[0].length == second.matrix.length) {
			result = new BitMatrix(this.matrix.length, second.matrix[0].length);
			for (int l1 = 0; l1 < this.matrix.length; l1++) {
				for (int c2 = 0; c2 < second.matrix[0].length; c2++) {
					boolean temp = false;
					for (int i = 0; i < second.matrix.length; i++) {
						temp ^= this.matrix[l1][i] & second.matrix[i][c2];
					}
					result.matrix[l1][c2] = temp;
				}
			}
		}
		return result;
	}

	public boolean[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(boolean[][] matrix) {
		this.matrix = matrix;
	}
}
