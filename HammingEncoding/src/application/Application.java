package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.border.TitledBorder;

import outils.OutilsHamming;
import outils.OutilsVarious;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;

@SuppressWarnings("javadoc")
public class Application extends JFrame {

	private JPanel contentPane;
	private JTextArea txtaOutput;
	private JSlider sldErrorChance;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Application() {
		setTitle("Hamming code");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnInfo = new JMenu("Info");
		menuBar.add(mnInfo);

		JMenuItem mntmNewMenuItem = new JMenuItem("About");
		mnInfo.add(mntmNewMenuItem);
		getContentPane().setLayout(null);

		JLabel lblTitle = new JLabel("Enter binary or text:");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblTitle.setBounds(10, 11, 590, 29);
		getContentPane().add(lblTitle);

		JScrollPane scrpInput = new JScrollPane();
		scrpInput.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrpInput.setBounds(10, 51, 764, 177);
		getContentPane().add(scrpInput);

		JTextArea txtaInput = new JTextArea();
		txtaInput.setLineWrap(true);
		scrpInput.setViewportView(txtaInput);

		JButton btnEncode = new JButton("Encode");
		btnEncode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtaOutput.setText(OutilsHamming.encode(txtaInput.getText(), sldErrorChance.getValue()));
			}
		});
		btnEncode.setBounds(10, 239, 89, 23);
		getContentPane().add(btnEncode);

		JButton btnDecode = new JButton("Decode");
		btnDecode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtaOutput.setText(OutilsHamming.decode(txtaInput.getText()));
			}
		});
		btnDecode.setBounds(123, 239, 89, 23);
		getContentPane().add(btnDecode);

		JScrollPane scrpOutput = new JScrollPane();
		scrpOutput.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrpOutput.setBounds(10, 307, 764, 221);
		getContentPane().add(scrpOutput);

		txtaOutput = new JTextArea();
		txtaOutput.setLineWrap(true);
		scrpOutput.setViewportView(txtaOutput);

		JLabel lblResult = new JLabel("Result:");
		lblResult.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblResult.setBounds(10, 273, 79, 23);
		getContentPane().add(lblResult);

		JLabel lblError = new JLabel("Encoding error chance (%):");
		lblError.setBounds(260, 243, 155, 14);
		getContentPane().add(lblError);

		sldErrorChance = new JSlider();
		sldErrorChance.setMajorTickSpacing(25);
		sldErrorChance.setMinorTickSpacing(5);
		sldErrorChance.setPaintTicks(true);
		sldErrorChance.setPaintLabels(true);
		sldErrorChance.setValue(0);
		sldErrorChance.setBounds(425, 236, 200, 45);
		getContentPane().add(sldErrorChance);

		JButton btnCopy = new JButton("Copy");
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OutilsVarious.saveToClipboard(txtaOutput.getText());
			}
		});
		btnCopy.setBounds(685, 273, 89, 23);
		getContentPane().add(btnCopy);

		JButton btnPaste = new JButton("Paste");
		btnPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtaInput.setText(OutilsVarious.pasteFromClipboard());
			}
		});
		btnPaste.setBounds(685, 18, 89, 23);
		getContentPane().add(btnPaste);

	}
}
